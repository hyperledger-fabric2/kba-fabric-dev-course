/* eslint-disable eqeqeq */
/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const shim = require('fabric-shim');

const crypto = require('crypto');
const myCollectionName = 'CollectionOne';

class BmwContract extends Contract {
    // PVT DATA
    async myPrivateAssetExists(ctx, myPrivateAssetId) {
        const buffer = await ctx.stub.getPrivateDataHash(myCollectionName, myPrivateAssetId);
        return (!!buffer && buffer.length > 0);
    }

    async createMyPrivateAsset(ctx, myPrivateAssetId) {
        const exists = await this.myPrivateAssetExists(ctx, myPrivateAssetId);
        if (exists) {
            throw new Error(`The asset my private asset ${myPrivateAssetId} already exists`);
        }

        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString('utf8');

        await ctx.stub.putPrivateData(myCollectionName, myPrivateAssetId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async readMyPrivateAsset(ctx, myPrivateAssetId) {
        const exists = await this.myPrivateAssetExists(ctx, myPrivateAssetId);
        if (!exists) {
            throw new Error(`The asset my private asset ${myPrivateAssetId} does not exist`);
        }
        let privateDataString;
        const privateData = await ctx.stub.getPrivateData(myCollectionName, myPrivateAssetId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

    async verifyMyPrivateAsset(ctx, myPrivateAssetId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(myCollectionName, myPrivateAssetId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + myPrivateAssetId);
        }

        const actualHash = pdHashBytes.toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }
    // PVT DATA END
    // =================== Manufacturer==================================//
    async bmwExists(ctx, bmwVin) {
        const buffer = await ctx.stub.getState(bmwVin);
        return (!!buffer && buffer.length > 0);
    }

    async addBmw(ctx, bmwVin, make, model, color, DOM, flag) {
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

        if(mspID == 'ManufacturerMSP'){
            const exists = await this.bmwExists(ctx, bmwVin);
            if (exists) {
                throw new Error(`A car with Vin number: ${bmwVin} already exists`);
            }
            const asset = {
                make,
                model,
                color,
                DOM,
                status: 'in Factory',
                flag,
                dealerName: 'not assigned'
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(bmwVin, buffer);

            let addCarEvent = {Type: 'car creation', Model: model};
            await ctx.stub.setEvent('addCarEvent', Buffer.from(JSON.stringify(addCarEvent)));
        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }

    async readBmw(ctx, bmwVin) {
        const exists = await this.bmwExists(ctx, bmwVin);
        if (!exists) {
            throw new Error(`A car with Vin number: ${bmwVin} does not exist`);
        }
        const buffer = await ctx.stub.getState(bmwVin);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async deleteBmw(ctx, bmwVin) {
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

        if(mspID == 'ManufacturerMSP'){
            const exists = await this.bmwExists(ctx, bmwVin);
            if (!exists) {
                throw new Error(`A car with Vin number: ${bmwVin} does not exist`);
            }
            await ctx.stub.deleteState(bmwVin);
        }
        else{
            logger.info('Users under the following MSP : '+
                        mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
            mspID + 'cannot perform this action');
        }
    }

    // =================== END Manufacturer==================================//

    // =================== Dealer ==================================//

    async ordExists(ctx, ordNum) {
        const buffer = await ctx.stub.getState(ordNum);
        return !!buffer && buffer.length > 0;
    }

    async raiseOrder(ctx, ordNum, make, model, color, dealerName) {
        const exists = await this.ordExists(ctx, ordNum);
        if (exists) {
            throw new Error(`An unmet order with same number: ${ordNum} already exists`);
        }

        const asset = { ordNum, make, model, color, dealerName };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(ordNum, buffer);
    }

    async readOrder(ctx, ordNum) {
        const exists = await this.ordExists(ctx, ordNum);
        if(!exists) {
            throw new Error(`An order with number: ${ordNum} does not exists`);

        }
        const buffer = await ctx.stub.getState(ordNum);
        const orderDetails = JSON.parse(buffer.toString());
        return orderDetails;
    }

    async deleteOrd(ctx, ordNum) {
        const exists = await this.ordExists(ctx, ordNum);
        if(!exists) {
            throw new Error(`Unable to delete since car with VIN number: ${ordNum} does not exists`);
        }
        return await ctx.stub.deleteState(ordNum);
    }

    async matchOrder(ctx, ordNum, bmwVin) {
        const exists = await this.bmwExists(ctx, bmwVin);
        if(!exists) {
            throw new Error(`A car with VIN number: ${bmwVin} does not exists`);
        }

        const orderExists = await this.ordExists(ctx, ordNum);
        if(!orderExists) {
            throw new Error(`Such an order with number: ${ordNum} does not exists`);
        }

        const carBuffer = await ctx.stub.getState(bmwVin);
        const carDetail = JSON.parse(carBuffer.toString());

        const orderBuffer = await ctx.stub.getState(ordNum);
        const ordDetail = JSON.parse(orderBuffer.toString());

        if(ordDetail.make === carDetail.make && ordDetail.model === carDetail.model &&
            ordDetail.color === carDetail.color){
            carDetail.dealerName = ordDetail.dealerName;
            carDetail.status = 'Assigned to dealer';
            carDetail.flag = 1;

            const newBuffer = Buffer.from(JSON.stringify(carDetail));
            await ctx.stub.putState(bmwVin, newBuffer);
            return await this.deleteOrd(ctx, ordNum);
        }
    }


    // =================== END Dealer==================================//
    async registerCar(ctx, bmwVin, ownName, plateNum) {
        let logger = shim.newLogger('Chaincode --> ');
        let CID = new shim.ClientIdentity(ctx.stub);
        let mspID = CID.getMSPID();
        logger.info('MSPID : '+ mspID);

        if(mspID == 'MvdMSP'){
            const exists = await this.bmwExists(ctx, bmwVin);
            if(!exists){
                throw new Error(`The car with Vin number: ${bmwVin} does not exists`);
            }

            const carBuffer = await ctx.stub.getState(bmwVin);
            const carDetail = JSON.parse(carBuffer.toString());

            const stat = 'Registered to: ' + ownName + ' with plate number: '+plateNum;

            carDetail.status = stat;
            carDetail.flag = 'on the road';

            const newbuffer = Buffer.from(JSON.stringify(carDetail));
            return await ctx.stub.putState(bmwVin, newbuffer);
        }
        else{
            logger.info('Users under the following MSP : '+
                    mspID + 'cannot perform this action');
            return('Users under the following MSP : '+
        mspID + 'cannot perform this action');
        }

    }

    // queryAllCars
    async queryAllCars(ctx) {

        const queryString = {
            selector: {

            }
        };


        // const queryString = {
        //     "selector": {
        //         "color": 'red'
        //     },
        //     "sort": [{"make": "desc"}]
        // };

        let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let results = await this.getAllResults(resultsIterator, false);

        return JSON.stringify(results);
    }
    // getCarsByRange
    async getCarsByRange(ctx, startKey, endKey) {

        let resultsIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let results = await this.getAllResults(resultsIterator, false);

        return JSON.stringify(results);
    }
    // getCarHistory
    async getCarHistory(ctx, bmwVin) {

        console.info('- start getCarHistory: %s\n', bmwVin);
        let resultsIterator = await ctx.stub.getHistoryForKey(bmwVin);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }
    // getCarsWithPagination
    async getCarsWithPagination(ctx, _pageSize, _bookmark) {

        const queryString = {
            selector: {

            }
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(JSON.stringify(queryString), pageSize, bookmark);
        const results = await this.getAllResults(iterator, false);
        // use RecordsCount and Bookmark to keep consistency with the go sample
        results.ResponseMetadata = {
            RecordsCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };

        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResults = [];
        // eslint-disable-next-line no-constant-condition
        while (true) {
            let res = await iterator.next();
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                console.log(res.value.value.toString('utf8'));

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.Timestamp = res.value.timestamp;
                    jsonRes.IsDelete = res.value.is_delete.toString();
                    try {
                        jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Value = res.value.value.toString('utf8');
                    }
                } else {
                    jsonRes.Key = res.value.key;
                    try {
                        jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                    } catch (err) {
                        console.log(err);
                        jsonRes.Record = res.value.value.toString('utf8');
                    }
                }
                allResults.push(jsonRes);
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return allResults;
            }
        }
    }

}

module.exports = BmwContract;
