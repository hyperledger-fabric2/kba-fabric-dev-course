var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('manufacturer', { title: 'Manufacturer Dash' });
});

router.get('/table', function(req, res, next) {
  res.render('table', { title: 'table' });
});
router.get('/event', function(req, res, next) {
  res.render('event', { title: 'Event' });
});

router.post('/manuwrite',function(req,res){

  const vin = req.body.VinNumb;
  const make = req.body.CarMake;
  const model = req.body.CarModel;
  const color = req.body.CarColor;
  const DOM = req.body.DOM;
  const flag = req.body.CarFlag;

  
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("manufacturer","admin")
  ManufacturerClient.initChannelAndChaincode("autochannel", "BMW");
  ManufacturerClient.generatedAndSubmitTxn(
      "addBmw",
      vin,make,model,color,DOM,flag
    )
    .then(message => {
      console.log(message);
    });
});

router.post('/manuread',async function(req,res){
  const Qvin = req.body.QVinNumb;
  let ManufacturerClient = new clientApplication();
  ManufacturerClient.setRoleAndIdentity("manufacturer","admin")
  ManufacturerClient.initChannelAndChaincode("autochannel", "BMW");
  ManufacturerClient.generatedAndSubmitTxn("readBmw", Qvin).then(message => {
    console.log(message.toString());
    res.send({ Cardata : message.toString() });
  });

 })



module.exports = router;


