let profile = {
    dealer: {
        "Wallet": "<path to network folder>/wallets/Dealer",
        "CCP": "<path to network folder>/gateways/Dealer/Dealer gateway.json"
    },
    mvd: {
        "Wallet": "<path to network folder>/wallets/Mvd",
        "CCP": "<path to network folder>/gateways/Mvd/Mvd gateway.json"
    },
    manufacturer: {
        "Wallet": "<path to network folder>/wallets/Manufacturer",
        "CCP": "<path to network folder>/gateways/Manufacturer/Manufacturer gateway.json"
    }
}

module.exports = {
    profile
}

