const {EventListner} = require('./Events')


let ManufacturerEvent = new EventListner();
ManufacturerEvent.setRoleAndIdentity("manufacturer","admin")
ManufacturerEvent.initChannelAndChaincode("autochannel", "BMW");

ManufacturerEvent.contractEventListner('SampleListner','addCarEvent')