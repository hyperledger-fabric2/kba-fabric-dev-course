const { clientApplication } = require("./client");

let ManufacturerClient = new clientApplication();
ManufacturerClient.setRoleAndIdentity("manufacturer","admin")
ManufacturerClient.initChannelAndChaincode("autochannel", "BMW");
ManufacturerClient.generatedAndSubmitTxn(
    "addBmw",
    "123",
    "S class",
    "Sedan",
    "red",
    "20-09-2020",
    "flag"
  )
  .then(message => {
    console.log(message);
  });
ManufacturerClient.generatedAndSubmitTxn("readBmw", "123").then(message => {
  console.log(message.toString());
});
