#
# SPDX-License-Identifier: Apache-2.0
#
---
- name: Deploy KBA Automobile blockchain infrastructure
  hosts: localhost
  vars:
    #
    # For information on these configuration options, read the documentation:
    # https://github.com/IBM-Blockchain/ansible-role-blockchain-platform-manager#example-playbook
    #
    infrastructure:
      type: docker
      docker:
        network: automobile

    organizations:
      - &Manufacturer
        msp:
          id: "ManufacturerMSP"
          admin:
            identity: "ManAdmin"
            secret: "ManAdminpw"
          ibp:
            display_name: "Manufacturer MSP"
        ca: &ManufacturerCA
          id: "ManufacturerCA"
          admin_identity: "admin"
          admin_secret: "adminpw"
          tls:
            enabled: false
          docker:
            name: ca.manufacturer.auto.com
            hostname: ca.manufacturer.auto.com
            port: 18050
          ibp:
            display_name: "Manufacturer CA"
        peers:
          - &ManufacturerPeer1
            id: "ManufacturerPeer1"
            identity: "manufacturerPeer1"
            secret: "manufacturerPeer1"
            database_type: leveldb
            tls:
              enabled: false
              identity: "org1peer1tls"
              secret: "org1peer1tlspw"
            docker:
              name: peer0.manufacturer.auto.com
              hostname: peer0.manufacturer.auto.com
              port: 18051
              chaincode_port: 18052
              operations_port: 18053
              couchdb:
                name: couchdb0.manufacturer.auto.com
                hostname: couchdb0.manufacturer.auto.com
                port: 18054
            ibp:
              display_name: "Manufacturer Peer1"
      
        nodes: "{{ playbook_dir }}/nodes/Manufacturer"
        wallet: "{{ playbook_dir }}/wallets/Manufacturer"
        gateways: "{{ playbook_dir }}/gateways/Manufacturer"
      - &Dealer
        msp:
          id: "DealerMSP"
          admin:
            identity: "DealerAdmin"
            secret: "DealerAdminpw"
          ibp:
            display_name: "Dealer MSP"
        ca: &DealerCA
          id: "DealerCA"
          admin_identity: "admin"
          admin_secret: "adminpw"
          tls:
            enabled: false
          docker:
            name: ca.dealer.auto.com
            hostname: ca.dealer.auto.com
            port: 19050
          ibp:
            display_name: "Dealer CA"
        peers:
          - &DealerPeer1
            id: "DealerPeer1"
            identity: "dealerpeer1"
            secret: "dealerpeer1pw"
            database_type: leveldb
            tls:
              enabled: false
              identity: "org2peer1tls"
              secret: "org2peer1tlspw"
            docker:
              name: peer0.dealer.auto.com
              hostname: peer0.dealer.auto.com
              port: 19051
              chaincode_port: 19052
              operations_port: 19053
              couchdb:
                name: couchdb0.dealer.auto.com
                hostname: couchdb0.dealer.auto.com
                port: 19054
            ibp:
              display_name: "Dealer Peer1"

        nodes: "{{ playbook_dir }}/nodes/Dealer"
        wallet: "{{ playbook_dir }}/wallets/Dealer"
        gateways: "{{ playbook_dir }}/gateways/Dealer"
      
      - &Mvd
        msp:
          id: "MvdMSP"
          admin:
            identity: "MvdAdmin"
            secret: "MvdAdminpw"
          ibp:
            display_name: "MVD MSP"
        ca: &MvdCA
          id: "MvdCA"
          admin_identity: "admin"
          admin_secret: "adminpw"
          tls:
            enabled: false
          docker:
            name: ca.mvd.auto.com
            hostname: ca.mvd.auto.com
            port: 20050
          ibp:
            display_name: "Mvd CA"
        peers:
          - &MvdPeer1
            id: "MvdPeer1"
            identity: "mvdpeer1"
            secret: "mvdpeer1pw"
            database_type: leveldb
            tls:
              enabled: false
              identity: "org2peer1tls"
              secret: "org2peer1tlspw"
            docker:
              name: peer0.mvd.auto.com
              hostname: peer0.mvd.auto.com
              port: 20051
              chaincode_port: 20052
              operations_port: 20053
              couchdb:
                name: couchdb0.mvd.auto.com
                hostname: couchdb0.mvd.auto.com
                port: 20054
            ibp:
              display_name: "Mvd Peer1"
  
        nodes: "{{ playbook_dir }}/nodes/Mvd"
        wallet: "{{ playbook_dir }}/wallets/Mvd"
        gateways: "{{ playbook_dir }}/gateways/Mvd"

      - &OrdererOrg
        msp:
          id: "OrdererMSP"
          admin:
            identity: "ordererAdmin"
            secret: "ordererAdminpw"
          ibp:
            display_name: "Orderer MSP"
        ca: &OrdererCA
          id: "OrdererCA"
          admin_identity: "admin"
          admin_secret: "adminpw"
          tls:
            enabled: false
          docker:
            name: ca.orderer.auto.com
            hostname: ca.orderer.auto.com
            port: 17050
          ibp:
            display_name: "Orderer CA"
        orderer: &Orderer
          id: "Orderer1"
          identity: "orderer1"
          secret: "orderer1pw"
          tls:
            enabled: false
            identity: "orderer1tls"
            secret: "orderer1tlspw"
          consortium:
            members:
              - *Manufacturer
              - *Dealer
              - *Mvd
          docker:
            name: orderer.auto.com
            hostname: orderer.auto.com
            port: 17051
            operations_port: 17052
          ibp:
            display_name: "Orderer1"
            cluster_name: "OrdererCluster"
        nodes: "{{ playbook_dir }}/nodes/Orderer"
        wallet: "{{ playbook_dir }}/wallets/Orderer"
        gateways: "{{ playbook_dir }}/gateways/Orderer"
    channels:
      - &autochannel
        name: autochannel
        orderer: *Orderer
        members:
          - <<: *Manufacturer
            committing_peers:
              - *ManufacturerPeer1
            anchor_peers:
              - *ManufacturerPeer1
          - <<: *Dealer
            committing_peers:
              - *DealerPeer1
            anchor_peers:
              - *DealerPeer1
          - <<: *Mvd
            committing_peers:
              - *MvdPeer1
            anchor_peers:
              - *MvdPeer1
    gateways:
      - name: Manufacturer gateway
        organization:
          <<: *Manufacturer
          gateway_peers:
            - <<: *ManufacturerPeer1
      - name: Dealer gateway
        organization:
          <<: *Dealer
          gateway_peers:
            - <<: *DealerPeer1
      - name: Mvd gateway
        organization:
          <<: *Mvd
          gateway_peers:
            - <<: *MvdPeer1
  roles:
    - ibm.blockchain_platform_manager
